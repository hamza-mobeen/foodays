import React, { Component } from 'react';
import {
    Card, CardImg, CardText, CardBody, CardTitle, BreadcrumbItem, Breadcrumb, Modal, ModalHeader,
    Label, ModalBody, Button, Row, Col
} from 'reactstrap';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';


const required = (val) => val && val.length > 0
const maxLength = (len) => (val) => !(val) || (val.length <= len)
const minLength = (len) => (val) => val && (val.length >= len)


class CommentForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isModalOpen: false
        }

        this.toggleModal = this.toggleModal.bind(this)
        this.handleCommentSubmit = this.handleCommentSubmit.bind(this)
    }

    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
    }

    handleCommentSubmit(values) {
        this.toggleModal();
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment)
    }

    render() {
        return (
            <div>
                <Button outline onClick={this.toggleModal}>
                    <span className="fa fa-pencil"> Submit Comment</span>
                </Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>
                        Submit Comment
                    </ModalHeader>
                    <ModalBody>


                        <LocalForm onSubmit={(values) => this.handleCommentSubmit(values)}>
                            <Row className="form-group">
                                <Col>
                                    <div>
                                        <Label htmlFor="name">Your Name</Label>
                                    </div>
                                    <div>
                                        <Control.text model=".author" id="name" name="name" placeholder="Your Name" className="form-control"
                                            validators={{
                                                required, minLength: minLength(2), maxLength: maxLength(15)
                                            }}
                                        />
                                        <Errors
                                            className="text-danger"
                                            model=".author"
                                            show="touched"
                                            messages={{
                                                required: 'This field is Required',
                                                minLength: 'Field should contain more than 2 characters',
                                                maxLength: 'Field should contain less than 15 characters'
                                            }}
                                        />
                                    </div>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col>
                                    <Label htmlFor="Rating">Rating</Label>
                                    <Control.select model=".rating" className="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>
                            </Row>
                            <Row className="form-group">

                                <Col>
                                    <br />
                                    <Label htmlFor="comment">Comment</Label>
                                    <div>
                                        <Control.textarea model=".comment" id="comment" name="comment"
                                            rows="12"
                                            className="form-control">
                                        </Control.textarea>
                                    </div>
                                    <div>
                                        <br />
                                        <Button type="submit" value="submit" color="primary">
                                            Submit
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </LocalForm>

                    </ModalBody>
                </Modal>
            </div>
        )
    }
}


function RenderComments({ comments, postComment, dishId }) {

    if (comments != null) {


        const commentList = comments.map((comment) => {

            return (
                <div>
                    <Fade in>
                        <p>{comment.comment}</p>
                        <p>-- {comment.author},
                            {new Intl.DateTimeFormat('en-US', {
                                year: 'numeric',
                                month: 'short',
                                day: '2-digit'
                            }).format(new Date(Date.parse(comment.date)))}</p>
                    </Fade>
                </div>
            );
        })
        


        return (
            <div>
                <h4>Comments</h4>

                {commentList}
                <div>
                    <CommentForm dishId={dishId} postComment={postComment} />
                </div>

            </div>
        );
    }
    else {
        return (
            <div></div>
        );
    }
}
function RenderDish({ dish }) {

    if (dish != null) {

        return (
            <FadeTransform in
                transformProps={{
                    exitTransform: 'scale(0.5) translateY(-50%)'
                }}>
                <Card>
                    <CardImg width="100%" object src={baseUrl + dish.image} alt={dish.name}></CardImg>
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>

                    </CardBody>
                </Card>
            </FadeTransform>

        );
    }
    else {
        return (

            <div></div>
        );
    }
}
const DishDetail = (props) => {

    if (props.isLoading) {
        return (
            <div className="container">

                <div className="row">
                    <Loading />

                </div>

            </div>

        );

    }
    else if (props.errMess) {
        return (
            <div className="container">

                <div className="row">
                    <h4>{props.errMess}</h4>

                </div>

            </div>
        );
    }

    else if (props.dish != null)
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12 ">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>

                </div>
                <div className="row">

                    <div className="col-12 col-md-5 m-1">
                        <RenderDish dish={props.dish} />
                    </div>
                    <div className="col-12 col-md-5 m-1">
                        <RenderComments comments={props.comments}
                            postComment={props.postComment}
                            dishId={props.dish.id} />
                    </div>

                </div>

            </div>
        );
    else
        return (
            <div></div>
        );
}



export default DishDetail;