import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createForms } from  'react-redux-form';
import { Dishes } from './dishes';
import { Leaders } from './leaders';
import { Comments } from './comments';
import { Promotions } from './promotions';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { InitialFeedback } from './Forms';

export const ConfigureStore = () => {

  const store = createStore(      // create store have second parameter as enhancer in which thunk and logger will be available in applymiddleware
    combineReducers({
      dishes: Dishes,
      promotions: Promotions,
      leaders: Leaders,
      comments: Comments,
      ...createForms({
        feedback: InitialFeedback
      })

    }),
    applyMiddleware(thunk, logger)
  );
  return store;
}